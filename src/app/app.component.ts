import {Component, OnInit} from '@angular/core';
import {SessionService} from "./services/storage/session.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private loggedIn = false;

  constructor(private sessionService: SessionService, private router: Router) {}

  isLoggedIn() {
    return this.loggedIn;
  }

  updateSession(status: boolean) {
    this.loggedIn = status;
    console.log(this.loggedIn)
    if(!status) {
      this.router.navigate(['/login']);
    }
  }

  ngOnInit() {
    this.loggedIn = this.sessionService.loadSession();
    this.sessionService.sessionChanged.subscribe(
      (data: boolean) => this.updateSession(data)
    );
    if(!this.loggedIn) {
      this.router.navigate(['/login']);
    }
  }

}

