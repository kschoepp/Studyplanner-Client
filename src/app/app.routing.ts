import {RouterModule, Routes} from "@angular/router";
import {DashboardComponent} from "./components/dashboard/dashboard.component";
import {TodoListComponent} from "./components/todolist/todolist.component";
import {LoginComponent} from "./components/login/login.component";
import {CalendarComponent} from "./components/calendar/calendar.component";
import {CoursesComponent} from "./components/courses/courses.component";
import {ExamsComponent} from "./components/exams/exams.component";
import {ContactsComponent} from "./components/contacts/contacts.component";
import {RegisterComponent} from "./components/register/register.component";


const APP_ROUTES: Routes = [
  { path: '', component: DashboardComponent},
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent},
  { path: 'todo', component: TodoListComponent},
  { path: 'calendar', component: CalendarComponent},
  { path: 'courses', component: CoursesComponent},
  { path: 'exams', component: ExamsComponent},
  { path: 'contacts', component: ContactsComponent }
];

export const AppRouter = RouterModule.forRoot(APP_ROUTES);
