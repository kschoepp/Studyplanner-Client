import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {SessionService} from "../storage/session.service";
import {ConfigService} from "../utils/config.service";

@Injectable()
export class UserService {
  rootURL = this.configService.getServerURL();
  user = this.sessionService.getUserData();
  id: number = this.sessionService.getUserID();

  constructor(private http: HttpClient, private sessionService: SessionService, private configService: ConfigService) {}

  //GET-Methods
  validateLogin(username: string, password: string) {
    //Creating JSON-Object from given paramters
    const user = {
      username: username,
      password: password
    };
    //Creating HTTP-GET-Request
    return this.http.get(this.rootURL + '/api/user/login', {params: user});
  }

  getUserDetails(username: string, password: string) {
    //Creating JSON-Object from given paramters
    const user = {
      username: username,
      password: password
    };
    //Creating HTTP-GET-Request
    return this.http.get(this.rootURL + '/api/user/details', {params: user});
  }

  getForeignUserDetails(username: string, password: string, user_two:string){
    const params = {
      username: username,
      password: password,
      user_two: user_two
    };

    return this.http.get(this.rootURL + '/api/user/foreigndetails', {params: params});
  }

  getUserById(id: number) {
    const params = {
      username: this.user["username"],
      password: this.user["password"],
      id: id.toString()
    };

    return this.http.get(this.rootURL + "/api/user/by/id", {params: params});
  }

  //POST-Methods
  registerUser(username: string, password: string, email: string, firstname: string, lastname: string, birthday: string, location: string) {

    //Create JSON body
    const body = {
      username: username,
      password: password,
      email: email,
      firstname: firstname,
      lastname: lastname,
      birthday: birthday,
      location: location
    };
    //Create HTTP-Header
    const headers = new HttpHeaders({'Content-Type': 'application/json'});
    //Creating HTTP-POST-Request
    return this.http.post(this.rootURL + "/api/user/register", body, {headers: headers});
  }

  //PATCH-Methods
  activateUser(key: string) {
    const params = {
      activation_key: key
    };

    return this.http.patch(this.rootURL + "/api/user/activate", null, {params: params});
  }

}
