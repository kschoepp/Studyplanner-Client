import {Injectable} from "@angular/core";
import {SessionService} from "../storage/session.service";
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../utils/config.service";
import {UserService} from "./user.service";

@Injectable()
export class ContactService {
  rootURL = this.configService.getServerURL();
  user = this.sessionService.getUserData();
  id = this.sessionService.getUserID();


  constructor(private sessionService: SessionService, private http: HttpClient, private configService: ConfigService, private userService: UserService) {}

  getAllContacts() {
    return this.http.get(this.rootURL + "/api/contact/by/user", {params: this.user});
  }

  deleteContact(id: number){
    const params = {
      username: this.user["username"],
      password: this.user["password"],
      id: id.toString()
    };

    return this.http.delete(this.rootURL + "/api/contact", {params: params});
  }

  confirmContact(id: number){
    const params = {
      username: this.user["username"],
      password: this.user["password"],
      id: id.toString()
    };

    return this.http.patch(this.rootURL + "/api/contact/confirm", null,{params: params});
  }

  createContact(user_two: string){

    const params= {
      username: this.user["username"],
      password: this.user["password"]
    };

    const body = {
      creator: this.id.toString(),
      user_one: this.id.toString(),
      user_two: user_two.toString()
    };

    return this.http.post(this.rootURL + "/api/contact", body, {params: params}  )
  }
}
