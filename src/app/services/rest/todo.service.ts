import {Injectable} from "@angular/core";
import {SessionService} from "../storage/session.service";
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../utils/config.service";

@Injectable()
export class TodoService {
  rootURL = this.configService.getServerURL();
  user = this.sessionService.getUserData();
  id: number = this.sessionService.getUserID();

  constructor(private sessionService: SessionService, private http: HttpClient, private configService: ConfigService) {}

  getTodos() {

    const user = {
      username: this.user["username"],
      password: this.user["password"]
    };

    return this.http.get(this.rootURL + '/api/todo/by/user', {params: user});
  }

  getTodoByKey(key: string) {
    const params = {
      username: this.user["username"],
      password: this.user["password"],
      key: key
    };

    return this.http.get(this.rootURL + "/api/todo/by/key", {params: params});
  }

  createTodo(key: string, task: string, priority: string) {

    let curDate: string = new Date().toDateString();

    const body = {
      key: key,
      task: task,
      owner: this.id,
      priority: priority,
      created: curDate
    };

    return this.http.post(this.rootURL + '/api/todo', body, {params: this.user});
  }

  patchTodoDone(key: string) {

    const params = {
      username: this.user["username"],
      password: this.user["password"],
      key: key
    };

    return this.http.patch(this.rootURL + "/api/todo/done", null, {params: params});
  }

  deleteTodo(key: string) {

    const params = {
      username: this.user["username"],
      password: this.user["password"],
      key: key
    };

    return this.http.delete(this.rootURL + "/api/todo", {params: params});
  }

}
