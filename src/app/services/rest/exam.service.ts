import {Injectable} from "@angular/core";
import {SessionService} from "../storage/session.service";
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../utils/config.service";

@Injectable()
export class ExamService {
  rootURL = this.configService.getServerURL();
  user = this.sessionService.getUserData();
  id: number = this.sessionService.getUserID();

  constructor(private sessionService: SessionService, private http: HttpClient, private configService: ConfigService) {}

  getExams() {
    return this.http.get(this.rootURL + "/api/exam/by/user", {params: this.user});
  }

  postExam(key: string, title: string, date_start: number, date_end: number, location: string) {

    const body = {
      username: this.user["username"],
      password: this.user["password"],
      key: key,
      title: title,
      owner: this.id,
      date_start: date_start,
      date_end: date_end,
      location: location
    };

    return this.http.post(this.rootURL + "/api/exam", body, {params: this.user});
  }

  deleteExam(key: string) {

    const params = {
      username: this.user["username"],
      password: this.user["password"],
      key: key
    };

    return this.http.delete(this.rootURL + "/api/exam", {params: params});
  }

}
