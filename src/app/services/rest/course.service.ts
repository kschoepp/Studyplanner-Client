import {Injectable} from "@angular/core";
import {SessionService} from "../storage/session.service";
import {ConfigService} from "../utils/config.service";
import {HttpClient} from "@angular/common/http";
import {KeyService} from "../utils/key.service";

@Injectable()
export class CourseService {

  rootURL = this.configService.getServerURL();
  user = this.sessionService.getUserData();
  id = this.sessionService.getUserID();

  constructor(private sessionService: SessionService, private http: HttpClient, private configService: ConfigService, private keyService: KeyService) {}

  getCoursesByUser() {
    return this.http.get(this.rootURL + "/api/course/by/user", {params: this.user});
  }

  getCoursesByKey(key: string) {
    const params = {
      username: this.user["username"],
      password: this.user["password"],
      key: key
    };

    return this.http.get(this.rootURL + "/api/course/by/key", {params: params});
  }

  postCourse(title: string, day: number, date_start: number, date_end: number, location: string) {
    const body = {
      key: this.keyService.generateKey(title),
      owner: this.id,
      title: title,
      day: day,
      date_start: date_start,
      date_end: date_end,
      location: location
    };

    return this.http.post(this.rootURL + "/api/course", body, {params: this.user});
  }

}
