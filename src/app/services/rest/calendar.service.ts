import {Injectable} from "@angular/core";
import {SessionService} from "../storage/session.service";
import {HttpClient} from "@angular/common/http";
import {ConfigService} from "../utils/config.service";

@Injectable()
export class CalendarService {
  rootURL = this.configService.getServerURL();
  user = this.sessionService.getUserData();
  id: number = this.sessionService.getUserID();

  constructor(private sessionService: SessionService, private http: HttpClient, private configService: ConfigService) {}

  getTodayAppointments() {

    let date = Date.now();

    const params = {
      username: this.user["username"],
      password: this.user["password"],
      owner: this.id.toString(),
      date: date.toString()
    };

    return this.http.get(this.rootURL + "/api/calendar/by/date", {params: params});
  }

  getAppointmentsByUser() {
    return this.http.get(this.rootURL + "/api/calendar/by/user", {params: this.user});
  }

  getAppointmentsByDate(date: Date) {

    const params = {
      username: this.user["username"],
      password: this.user["password"],
      owner: this.id.toString(),
      date: date.getTime().toString()
    };

    return this.http.get(this.rootURL + "/api/calendar/by/date", {params: params});
  }

  createAppointment(key: string, topic: string, dateStart: number, dateEnd: number, location: string, participants: string) {

    const body = {
      key: key,
      topic: topic,
      owner: this.id,
      date_start: dateStart,
      date_end: dateEnd,
      location: location,
      participants: participants
    };

    return this.http.post(this.rootURL + "/api/calendar", body, {params: this.user});
  }

  deleteAppointment(key: string) {

    const params = {
      username: this.user["username"],
      password: this.user["password"],
      key: key
    };

    return this.http.delete(this.rootURL + "/api/calendar", {params: params});
  }

}
