import {EventEmitter, Injectable} from "@angular/core";

@Injectable()
export class SessionService {
  private id: number;
  private username: string;
  private password: string;
  private firstname: string;
  private lastname: string;
  private email: string;

  sessionChanged = new EventEmitter();

  constructor() {
    this.username = localStorage.getItem("username");
    this.password = localStorage.getItem("password");
    this.id = Number(localStorage.getItem("id"));
    this.update();
  }

  loadSession() {
    return (this.username != undefined && this.password != undefined);
  }

  createSession(id: number, username: string, password: string, firstname: string, lastname: string, email: string) {
    this.id = id;
    this.username = username;
    this.password = password;
    this.firstname = firstname;
    this.lastname = lastname;
    this.email = email;
    localStorage.setItem("id", String(id));
    localStorage.setItem("username", username);
    localStorage.setItem("password", password);
    localStorage.setItem("firstname", firstname);
    localStorage.setItem("lastname", lastname);
    localStorage.setItem("email", email);
    this.update();
  }

  deleteSession() {
    this.username = undefined;
    this.password = undefined;
    localStorage.clear();
    this.update();
  }

  update() {
    let status: boolean = false;
    if(this.username != undefined && this.password != undefined) {
      status = true;
    }
    this.sessionChanged.emit(status);
  }

  getUserData() {
    return {
      username: this.username,
      password: this.password
    }
  }

  getUserID() {
    return this.id;
  }

}
