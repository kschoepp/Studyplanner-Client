import {Injectable} from "@angular/core";

@Injectable()
export class ConfigService {

  private serverURL = "http://ingence.de:10080";

  getServerURL() {
    return this.serverURL;
  }

}
