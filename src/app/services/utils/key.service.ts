import {Md5} from "ts-md5";
import {Injectable} from "@angular/core";

@Injectable()
export class KeyService {

  generateKey(value: string) {
    let rndNumber: number = Math.floor(Math.random() * (Math.random() - Math.random() + 1)) + Math.random();
    let key: string = value + rndNumber;
    let eKey: string = Md5.hashStr(key).toString();
    return eKey;
  }

}
