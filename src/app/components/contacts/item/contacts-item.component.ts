import {Component, EventEmitter, Input, OnInit} from "@angular/core";
import {ContactService} from "../../../services/rest/contact.service";
import {SessionService} from "../../../services/storage/session.service";
import {UserService} from "../../../services/rest/user.service";

@Component({
  selector: 'app-contacts-item',
  templateUrl: './contacts-item.component.html',
  styleUrls: ['./contacts-item.component.css']
})

export class ContactsItemComponent implements OnInit {
  @Input() changeEmitter: EventEmitter<boolean>;
  @Input() id: number;
  @Input() user_one: number;
  @Input() user_two: number;
  @Input() isConfirmed: boolean;
  username: string;
  firstname: string;
  lastname: string;
  email: string;
  birthday: string;
  location: string;
  private ownID: number;

  constructor(private contactService: ContactService, private sessionService: SessionService, private userService: UserService) { }

  ngOnInit() {
    this.ownID = this.sessionService.getUserID();
    this.loadContactDetails();

  }

  loadContactDetails(){
    let ID;
    if(this.ownID == this.user_one){
      ID = this.user_two;
    } else {
      ID = this.user_one;
    }
    this.userService.getUserById(ID).subscribe(
      (data:JSON) =>  this.loadValues(data),
      error => console.log(error)
    );
  }

  loadValues(data:JSON){
    this.username = data["username"];
    this.email = data["email"];
    this.firstname = data["firstname"];
    this.lastname = data["lastname"];
    this.birthday = data["birthday"];
    this.location = data["location"];
    if (this.birthday == undefined || this.birthday == ""){
      this.birthday = "-";
    }
    if(this.location == undefined || this.location == ""){
      this.location = "-";
    }
  }

  deleteContact(){
    this.contactService.deleteContact(this.id).subscribe(
      data => this.changeEmitter.emit(true),
      error => console.log(error)
    );

  }

  setConfirmed(){
    this.contactService.confirmContact(this.id).subscribe(
      data => this.changeEmitter.emit(true),
      error => console.log(error)
    );
  }
}

