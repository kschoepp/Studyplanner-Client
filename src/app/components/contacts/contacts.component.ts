import {Component, EventEmitter, OnInit} from "@angular/core";
import {ContactService} from "../../services/rest/contact.service";
import {NgForm} from "@angular/forms";
import {UserService} from "../../services/rest/user.service";
import {SessionService} from "../../services/storage/session.service";

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  providers: [ContactService, UserService]
})

export class ContactsComponent implements OnInit {

  allContacts: Array<JSON>;
  confirmedContacts: Array<JSON>;
  unconfirmedContacts: Array<JSON>;
  user = this.sessionService.getUserData();
  id = this.sessionService.getUserID();
  changeEmitter: EventEmitter<boolean>;

  constructor(private contactService: ContactService, private userService: UserService, private sessionService: SessionService) {}

  ngOnInit() {
    this.allContacts = [];
    this.confirmedContacts = [];
    this.unconfirmedContacts = [];
    this.changeEmitter = new EventEmitter<boolean>();
    this.changeEmitter.subscribe(
      event => this.reloadContacts(),
      error => console.log(error)
    );
    this.contactService.getAllContacts().subscribe(
      (data: Array<JSON>) => this.loadContacts(data),
      error => console.log(error)
    );
  }

  reloadContacts(){
    this.contactService.getAllContacts().subscribe(
      (data:Array<JSON>) => this.loadContacts(data)
    );
  }

  loadContacts(data: Array<JSON>) {
    data = data.slice(1, data.length);
    this.allContacts = [];
    this.confirmedContacts = [];
    this.unconfirmedContacts = [];
    this.allContacts = data;
    for(let i in this.allContacts) {
      let curContact: JSON = this.allContacts[i];
      if(curContact["confirmed"] == true) {
        this.confirmedContacts.push(curContact);
      } else {
        if(curContact["creator"] != this.sessionService.getUserID()) {
          this.unconfirmedContacts.push(curContact);
        }
      }
    }
  }

  createContact(form: NgForm){
    let user_two: string = form.value["username"];
    this.userService.getForeignUserDetails(this.user["username"], this.user["password"], user_two).subscribe(
      (data:JSON) => {
        this.contactService.createContact(data["id"]).subscribe(
          data => {
            form.resetForm();
            this.reloadContacts();
          },
          error => console.log(error)
        );
      },
      error => console.log(error)
    );

  }
}
