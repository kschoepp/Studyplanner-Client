import {Component, EventEmitter, OnInit} from "@angular/core";
import {ExamService} from "../../services/rest/exam.service";
import {NgForm} from "@angular/forms";
import {KeyService} from "../../services/utils/key.service";

@Component({
  selector: 'app-exams',
  templateUrl: './exams.component.html',
  providers: [ExamService]

})

export class ExamsComponent implements OnInit{
  items: Array<JSON>;
  changeEmitter: EventEmitter<boolean>;

  constructor(private examService: ExamService, private keyService: KeyService){}

  ngOnInit(){
    this.changeEmitter = new EventEmitter<boolean>();
    this.changeEmitter.subscribe(
      data => this.loadExams(),
      error => console.log(error)
    );
    this.examService.getExams().subscribe(
      (data:Array<JSON>) => this.setItems(data),
      error => console.log(error)
    );
  }

  loadExams(){
    this.examService.getExams().subscribe(
      (data:Array<JSON>) => this.setItems(data),
      error => console.log(error)
    );
  }

  createExamItem(form: NgForm){
    let title: string = form.value["title"];
    let dateS: string = form.value["date_start"];
    let dateE: string = form.value["date_end"];
    let timeS: string = form.value["time_start"];
    let timeE: string = form.value["time_end"];
    let location: string = form.value["location"];
    let dateStart = new Date(dateS.substring(6,10) + "-" + dateS.substring(3,5) + "-" + dateS.substring(0,2));
    dateStart.setHours(Number(timeS.substring(0,2)));
    dateStart.setMinutes(Number(timeS.substring(3,5)));
    let dateEnd = new Date(dateE.substring(6,10) + "-" + dateE.substring(3,5) + "-" + dateE.substring(0,2));
    dateEnd.setHours(Number(timeE.substring(0,2)));
    dateEnd.setMinutes(Number(timeE.substring(3,5)));
    this.examService.postExam(this.keyService.generateKey(title), title, dateStart.getTime(), dateEnd.getTime(), location).subscribe(
      (data: JSON) => this.examCreated(data, form),
      error => console.log(error)
    );
  }

  examCreated(data:JSON, form: NgForm){
    if(data["httpCode"] == 200){
      form.resetForm();
      this.loadExams();
    }
  }

  setItems(data:Array<JSON>){
    data = data.slice(1, data.length);
    console.log(data);
    this.items = data;
  }
}
