import {Component, EventEmitter, Input} from "@angular/core";
import {ExamService} from "../../../services/rest/exam.service";

@Component({
  selector: 'app-exam-item',
  templateUrl: './exam-item.component.html',
  styleUrls: ['./exam-item.component.css']
})

export class ExamItemComponent {
  @Input() key: string;
  @Input() title: string;
  @Input() date_start: number;
  @Input() date_end: number;
  @Input() location: string;
  @Input() changeEmitter: EventEmitter<boolean>;
  dateStart: string;
  dateEnd: string;

  constructor (private examService: ExamService){}

  ngOnInit(){
    if(this.location == undefined || this.location == ""){
      this.location = "-";
    }
    let dateStartObject = new Date(this.date_start);
    this.dateStart = this.convert(dateStartObject.getDate())
      + "-" + this.convert(dateStartObject.getMonth()+1)
      + "-" + this.convert(dateStartObject.getFullYear())
      + " " + this.convert(dateStartObject.getHours())
      + ":" + this.convert(dateStartObject.getMinutes());
    let dateEndObject = new Date(this.date_end);
    this.dateEnd = this.convert(dateEndObject.getDate())
      + "-" + this.convert(dateEndObject.getMonth()+1)
      + "-" + this.convert(dateEndObject.getFullYear())
      + " " + this.convert(dateEndObject.getHours())
      + ":" + this.convert(dateEndObject.getMinutes());
  }

  convert(n){
    return n > 9 ? "" + n: "0" + n;
  }

  deleteExam(){
    this.examService.deleteExam(this.key).subscribe(
      (data:JSON) => this.removeItem(data),
      error => console.log(error)
    );
  }

  removeItem(data:JSON){
    console.log("hi");
    if(data["httpCode"] == 200) {
      this.changeEmitter.emit(true);
    }
  }
}
