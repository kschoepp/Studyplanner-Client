import {Component, EventEmitter, Input, OnInit} from "@angular/core";
import {TodoService} from "../../../services/rest/todo.service";

@Component({
  selector: 'app-todo-list-item',
  templateUrl: './todolist-item.component.html',
  styleUrls: ['./todolist-item.component.css'],
  providers: [TodoService]
})

export class TodoListItemComponent implements OnInit {
  @Input() selectEmitter: EventEmitter<string>;
  @Input() doneEmitter: EventEmitter<string>;
  @Input() key: string;
  @Input() title: string;
  @Input() priority: string;
  @Input() due: string;

  constructor(private todoService: TodoService) {}

  ngOnInit() {
    if(this.due == undefined) {
      this.due = "-";
    }
  }

  setDone() {
    this.todoService.patchTodoDone(this.key).subscribe(
      (data: JSON) => this.removeItem(data),
      error => console.log(error)
    );
  }

  selectEmit() {
    this.selectEmitter.emit(this.key);
  }

  removeItem(data: JSON) {
    if(data["httpCode"] == 200) {
      this.doneEmitter.emit(this.key);
    }
  }

}
