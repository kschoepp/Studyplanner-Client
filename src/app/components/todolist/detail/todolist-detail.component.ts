import {Component, EventEmitter, Input, OnInit} from "@angular/core";
import {UserService} from "../../../services/rest/user.service";
import {TodoService} from "../../../services/rest/todo.service";

@Component({
  selector: 'app-todo-list-detail',
  templateUrl: './todolist-detail.component.html',
  providers: [UserService, TodoService]
})

export class TodolistDetailComponent implements OnInit {
  @Input() key: string;
  @Input() detailEmitter: EventEmitter<string>;
  @Input() changeEmitter: EventEmitter<boolean>;
  task: string;
  owner: number;
  priority: string;
  //due: number;
  done: boolean;
  created: string;
  //participants: string;
  ownerName: string;

  constructor(private userService: UserService, private todoService: TodoService) {}

  ngOnInit() {
    this.task = "-";
    this.owner = 0;
    this.ownerName = "-";
    this.priority = "-";
    this.created = "-";
    this.done = false;

    this.todoService.getTodoByKey(this.key).subscribe(
      (data: JSON) => this.loadData(data),
      error => console.log(error)
    );

    this.detailEmitter.subscribe(
      data => this.refresh(data),
      error => console.log(error)
    );
  }

  refresh(data: string) {
    this.key = data;
    this.todoService.getTodoByKey(this.key).subscribe(
      (data: JSON) => this.loadData(data),
      error => console.log(error)
    );
  }

  loadData(data: JSON) {
    this.task = data["task"];
    this.owner = data["owner"];
    this.priority = data["priority"];
    this.created = data["created"];
    this.done = data["done"];

    this.userService.getUserById(this.owner).subscribe(
      (data: JSON) => this.ownerName = data["firstname"] + " " + data["lastname"],
      error => console.log(error)
    );
  }

  onClickDone() {
    this.todoService.patchTodoDone(this.key).subscribe(
      data => this.changeEmitter.emit(true),
      error => console.log(error)
    );
  }


  onClickDelete() {
    this.todoService.deleteTodo(this.key).subscribe(
      data => this.changeEmitter.emit(true),
      error => console.log(error)
    );
  }

}
