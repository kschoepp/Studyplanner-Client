import {Component, EventEmitter, Input, OnInit} from "@angular/core";
import {TodoService} from "../../services/rest/todo.service";
import {NgForm} from "@angular/forms";
import {KeyService} from "../../services/utils/key.service";

@Component({
  selector: 'app-todo-list',
  templateUrl: './todolist.component.html',
  styleUrls: ['./todolist.component.css'],
  providers: [TodoService]
})

export class TodoListComponent implements OnInit {

  items: Array<JSON>;
  itemSelected: boolean;
  selectedItem: string;
  itemEmitter: EventEmitter<string>;
  detailEmitter: EventEmitter<string>;
  changeEmitter: EventEmitter<boolean>;
  doneEmitter: EventEmitter<string>;

  constructor(private todoService: TodoService, private keyService: KeyService) {
  }

  ngOnInit() {
    this.itemSelected = false;
    this.itemEmitter = new EventEmitter<string>();
    this.detailEmitter = new EventEmitter<string>();
    this.changeEmitter = new EventEmitter<boolean>();
    this.doneEmitter = new EventEmitter<string>();
    this.itemEmitter.subscribe(
      event => this.loadDetailItem(event),
      error => console.log(error)
    );
    this.changeEmitter.subscribe(
      event => this.detailAction(event),
      error => console.log(error)
    );
    this.todoService.getTodos().subscribe(
      (data: Array<JSON>) => this.setItems(data)
    );
    this.doneEmitter.subscribe(
      event => this.loadItem(),
      error => console.log(error)
    );
  }

  loadItem() {
    this.todoService.getTodos().subscribe(
      (data: Array<JSON>) => this.setItems(data)
    );
  }

  loadDetailItem(itemKey: string) {
    this.selectedItem = itemKey;
    this.itemSelected = true;
    this.detailEmitter.emit(itemKey);
  }

  createTodoItem(form: NgForm) {
    let task: string = form.value["task"];
    let key: string = this.keyService.generateKey(task);
    let priority: string = form.value["priority"];
    console.log(priority);
    if(priority == "" || priority == null) {
      priority = "medium";
    }

    this.todoService.createTodo(key, task, priority).subscribe(
      (data: JSON) => this.todoCreated(data, form),
      error => console.log(error)
    );
  }

  detailAction(event: boolean) {
    if(event) {
      this.itemSelected = false;
      this.updateTodos();
    }
  }

  todoCreated(data: JSON, form: NgForm) {
    if(data["httpCode"] == 200) {
      form.resetForm();
    }
    this.updateTodos();
  }

  updateTodos() {
    this.todoService.getTodos().subscribe(
      (data: Array<JSON>) => this.setItems(data)
    );
  }

  setItems(data: Array<JSON>) {
    data = data.slice(1, data.length);
    this.items = data;
  }

}
