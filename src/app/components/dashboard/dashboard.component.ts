import {Component, OnInit} from "@angular/core";
import {TodoService} from "../../services/rest/todo.service";
import {CalendarService} from "../../services/rest/calendar.service";
import {NgForm} from "@angular/forms";
import {KeyService} from "../../services/utils/key.service";
import {ExamService} from "../../services/rest/exam.service";
import {CourseService} from "../../services/rest/course.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  providers: [CalendarService, TodoService, ExamService, CourseService]
})

export class DashboardComponent implements OnInit {

  aptTodayCount: number = 0;
  todoCount: number = 0;
  examCount: number = 0;
  courseCount: number = 0;

  constructor(private calendarService: CalendarService ,private todoService: TodoService, private courseService: CourseService, private examService: ExamService, private keyService: KeyService) {}

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.getTodayAppointments();
  }

  getTodayAppointments() {
    this.calendarService.getTodayAppointments().subscribe(
      (data: Array<JSON>) => {
        this.aptTodayCount = data.length-1;
        this.getOpenTodos();
      },
      error => console.log(error)
    );
  }

  getOpenTodos() {
    this.todoService.getTodos().subscribe(
      (data: Array<JSON>) => {
        this.todoCount = data.length-1;
        this.getAllCourses();
      },
      error => console.log(error)
    );
  }

  getAllCourses() {
    this.courseService.getCoursesByUser().subscribe(
      (data: Array<JSON>) => {
        this.courseCount = data.length-1;
        this.getOpenExams();
      },
      error => console.log(error)
    );
  }

  getOpenExams() {
    this.examService.getExams().subscribe(
      (data: Array<JSON>) => this.examCount = data.length-1,
      error => console.log(error)
    );
  }

  createTodoItem(form: NgForm) {
    let task: string = form.value["task"];
    let key: string = this.keyService.generateKey(task);
    let priority: string = form.value["priority"];
    if(priority == "") {
      priority = "medium";
    }

    this.todoService.createTodo(key, task, priority).subscribe(
      (data: JSON) => this.todoCreated(data, form),
      error => console.log(error)
    );
  }

  todoCreated(data: JSON, form: NgForm) {
    if(data["httpCode"] == 200) {
      form.resetForm();
      this.getTodayAppointments();
    }
  }

  createAppointment(form: NgForm) {
    let description: string = form.value["description"];
    let date: string = form.value["date"];
    let time: string = form.value["time"];
    let location: string = form.value["location"];
    let dateStart = new Date(date.substring(6,10) + "-" + date.substring(3,5) + "-" + date.substring(0,2));
    dateStart.setHours(Number(time.substring(0,2)));
    dateStart.setMinutes(Number(time.substring(3,5)));
    let dateEnd = new Date(date.substring(6,10) + "-" + date.substring(3,5) + "-" + date.substring(0,2));;
    dateEnd.setHours(Number(time.substring(0,2))+1);
    dateEnd.setMinutes(Number(time.substring(3,5)));
    console.log(dateStart.getTime());
    this.calendarService.createAppointment(this.keyService.generateKey(description), description, dateStart.getTime(), dateEnd.getTime(), location, "").subscribe(
      (data: JSON) => this.aptCreated(data, form),
      error => console.log(error)
    );
  }

  aptCreated(data: JSON, form: NgForm) {
    if(data["httpCode"] == 200) {
      form.resetForm();
      this.getTodayAppointments();
    }
  }

}
