import { Component } from "@angular/core";
import {SessionService} from "../../services/storage/session.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent {

  constructor(private sessionService: SessionService) {}

  logout() {
    this.sessionService.deleteSession();
  }

}
