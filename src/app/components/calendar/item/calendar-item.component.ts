import {Component, EventEmitter, Input, OnInit} from "@angular/core";
import {CalendarService} from "../../../services/rest/calendar.service";

@Component({
  selector: 'app-calendar-item',
  templateUrl: './calendar-item.component.html',
  providers: [CalendarService]
})

export class CalendarItemComponent implements OnInit {
  @Input() key;
  @Input() title;
  @Input() dateStartNumber;
  @Input() dateEndNumber;
  @Input() location;
  @Input() changeEmitter: EventEmitter<string>;
  dateStart: string;
  dateEnd: string;

  constructor(private calendarService: CalendarService) {}

  ngOnInit() {
    if(this.location == undefined || this.location == "") {
      this.location = "-";
    }
    let dateStartObject = new Date(this.dateStartNumber);
    this.dateStart = this.convert(dateStartObject.getDate())
      + "-" + this.convert(dateStartObject.getMonth()+1)
      + "-" + this.convert(dateStartObject.getFullYear())
      + " " + this.convert(dateStartObject.getHours())
      + ":" + this.convert(dateStartObject.getMinutes());
    let dateEndObject = new Date(this.dateEndNumber);
    this.dateEnd = this.convert(dateEndObject.getDate())
      + "-" + this.convert(dateEndObject.getMonth()+1)
      + "-" + this.convert(dateEndObject.getFullYear())
      + " " + this.convert(dateEndObject.getHours())
      + ":" + this.convert(dateEndObject.getMinutes());

  }

  convert(n){
    return n > 9 ? "" + n: "0" + n;
  }

  deleteAppointment() {
    this.calendarService.deleteAppointment(this.key).subscribe(
      data => this.changeEmitter.emit(this.key),
      error => console.log(error)
    );
  }

}
