import {Component, EventEmitter, OnInit} from "@angular/core";
import {CalendarService} from "../../services/rest/calendar.service";
import {KeyService} from "../../services/utils/key.service";
import {CalendarEvent} from 'angular-calendar';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css'],
  providers: [CalendarService]
})

export class CalendarComponent implements OnInit {

  events: CalendarEvent[];
  items: Array<JSON>;
  initSelectedDate: boolean;
  selectedDate: Date;
  selectedDateEmitter: EventEmitter<Date>;
  changeEmitter: EventEmitter<string>;
  blue: any;

  constructor(private calendarService: CalendarService, private keyService: KeyService) {}

  ngOnInit() {
    this.events = [];
    this.blue = {
      primary: '#1e90ff',
      secondary: '#D1E8FF'
    };
    this.selectedDate = new Date();
    this.initSelectedDate = false;
    this.selectedDateEmitter = new EventEmitter<Date>();
    this.changeEmitter = new EventEmitter<string>();
    this.calendarService.getTodayAppointments().subscribe(
      (data: Array<JSON>) => this.setItems(data),
      error => console.log(error)
    );
    this.calendarService.getAppointmentsByUser().subscribe(
      (data: Array<JSON>) => this.loadEvents(data),
      error => console.log(error)
    );
    this.changeEmitter.subscribe(
      data => this.loadAppointments(),
      error => console.log(error)
    );
  }

  loadEvents(data: Array<JSON>) {
    data = data.slice(1, data.length);
    this.events = [];
    for(let i in data) {
      let curData: JSON = data[i];
      this.events.push({
        title: curData["topic"],
        start: new Date(curData["date_start"]),
        end: new Date(curData["date_end"]),
        color: this.blue
      });
    }
    if(!this.initSelectedDate) {
      this.selectedDate = new Date();
      this.initSelectedDate = true;
    }
  }

  loadAppointments() {
    this.calendarService.getAppointmentsByDate(this.selectedDate).subscribe(
      (data: Array<JSON>) => this.reloadAppointsments(data),
      error => console.log(error)
    );
  }

  reloadAppointsments(data: Array<JSON>) {
    this.setItems(data);
    this.calendarService.getAppointmentsByUser().subscribe(
      (data: Array<JSON>) => this.loadEvents(data),
      error => console.log(error)
    );
  }

  createAppointment(form: NgForm) {
    let description: string = form.value["description"];
    let dateS: string = form.value["date_start"];
    let dateE: string = form.value["date_end"];
    let timeS: string = form.value["time_start"];
    let timeE: string = form.value["time_end"];
    let location: string = form.value["location"];
    let dateStart = new Date(dateS.substring(6,10) + "-" + dateS.substring(3,5) + "-" + dateS.substring(0,2));
    dateStart.setHours(Number(timeS.substring(0,2)));
    dateStart.setMinutes(Number(timeS.substring(3,5)));
    let dateEnd = new Date(dateE.substring(6,10) + "-" + dateE.substring(3,5) + "-" + dateE.substring(0,2));
    dateEnd.setHours(Number(timeE.substring(0,2)));
    dateEnd.setMinutes(Number(timeE.substring(3,5)));
    this.calendarService.createAppointment(this.keyService.generateKey(description), description, dateStart.getTime(), dateEnd.getTime(), location, "").subscribe(
      (data: JSON) => this.aptCreated(data, form),
      error => console.log(error)
    );
  }

  aptCreated(data: JSON, form: NgForm) {
    if(data["httpCode"] == 200) {
      form.resetForm();
      this.loadAppointments();
    }
  }

  dateSelected(newDate: Date) {
    this.selectedDate = newDate;
    this.loadAppointments();
  }

  setItems(data: Array<JSON>) {
    data = data.slice(1, data.length);
    this.items = data;
  }

}
