import {Component, OnInit} from "@angular/core";
import {CourseService} from "../../services/rest/course.service";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css'],
  providers: [CourseService]
})

export class CoursesComponent implements OnInit {

  selectedDate: Date;
  mondayCourses: Array<JSON>;
  tuesdayCourses: Array<JSON>;
  wednesdayCourses: Array<JSON>;
  thursdayCourses: Array<JSON>;
  fridayCourses: Array<JSON>;
  saturdayCourses: Array<JSON>;

  constructor(private courseService: CourseService) {}

  ngOnInit() {
    this.selectedDate = new Date();
    this.mondayCourses = [];
    this.tuesdayCourses = [];
    this.wednesdayCourses = [];
    this.thursdayCourses = [];
    this.fridayCourses = [];
    this.saturdayCourses = [];
    this.loadCourses();
  }

  loadCourses() {
    this.courseService.getCoursesByUser().subscribe(
      (data: Array<JSON>) => this.setCourses(data),
      error => console.log(error)
    );
  }

  setCourses(data: Array<JSON>) {
    data = data.slice(1, data.length);
    this.mondayCourses = [];
    this.tuesdayCourses = [];
    this.wednesdayCourses = [];
    this.thursdayCourses = [];
    this.fridayCourses = [];
    this.saturdayCourses = [];
    for(let i in data) {
      let curCourse: JSON = data[i];
      if(curCourse["day"] == 0) {
        this.mondayCourses.push(curCourse);
      } else if(curCourse["day"] == 1) {
        this.tuesdayCourses.push(curCourse);
      } else if(curCourse["day"] == 2) {
        this.wednesdayCourses.push(curCourse);
      } else if(curCourse["day"] == 3) {
        this.thursdayCourses.push(curCourse);
      } else if(curCourse["day"] == 4) {
        this.fridayCourses.push(curCourse);
      } else if(curCourse["day"] == 5) {
        this.saturdayCourses.push(curCourse);
      }
    }
  }

  createCourse(form: NgForm) {
    let title: string = form.value["title"];
    let location: string = form.value["location"];
    let dayString: string = form.value["day"];
    let timeS: string = form.value["time_start"];
    let timeE: string = form.value["time_end"];
    let dateStart = new Date();
    dateStart.setHours(Number(timeS.substring(0,2)));
    dateStart.setMinutes(Number(timeS.substring(3,5)));
    let dateEnd = new Date();
    dateEnd.setHours(Number(timeE.substring(0,2)));
    dateEnd.setMinutes(Number(timeE.substring(3,5)));
    let day: number;
    if(dayString == "Monday") {
      day = 0;
    } else if(dayString == "Tuesday") {
      day = 1;
    } else if(dayString == "Wednesday") {
      day = 2;
    } else if(dayString == "Thursday") {
      day = 3;
    } else if(dayString == "Friday") {
      day = 4;
    } else {
      day = 5;
    }

    this.courseService.postCourse(title, day, dateStart.getTime(), dateEnd.getTime(), location).subscribe(
      (data: JSON) => {
        this.loadCourses();
        form.resetForm();
      },
      error => console.log(error)
    );
  }

}
