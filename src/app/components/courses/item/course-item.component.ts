import {Component, Input, OnInit} from "@angular/core";

@Component({
  selector: 'app-course-item',
  templateUrl: './course-item.component.html'
})

export class CourseItemComponent implements OnInit {
  @Input() key: string;
  @Input() title: string;
  @Input() location: string;
  @Input() dateStart: number;
  @Input() dateEnd: number;
  start: string;
  end: string;

  ngOnInit() {
    if(this.location == undefined || this.location == "") {
      this.location = "-";
    }
    this.start = "-";
    this.end = "-";
    let dateStartObject = new Date(this.dateStart);
    this.start = this.convert(dateStartObject.getHours()) + ":" + this.convert(dateStartObject.getMinutes());
    let dateEndObject = new Date(this.dateEnd);
    this.end = this.convert(dateEndObject.getHours()) + ":" + this.convert(dateEndObject.getMinutes());
  }

  convert(n){
    return n > 9 ? "" + n: "0" + n;
  }

}
