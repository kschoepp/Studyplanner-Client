import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";
import {UserService} from "../../services/rest/user.service";
import {NgForm} from "@angular/forms";
import {Md5} from "ts-md5";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [UserService]
})

export class RegisterComponent implements OnInit {

  showActivationButton: boolean;
  activationKey: string;

  constructor(private userService: UserService, private router: Router) {}

  ngOnInit() {
    this.showActivationButton = false;
  }

  onSubmit(form: NgForm) {
    const username: string = form.value["username"];
    const firstname: string = form.value["firstname"];
    const lastname: string = form.value["lastname"];
    const email: string = form.value["email"];
    const password: string = form.value["password"];
    const passwordConfirm: string = form.value["password-confirm"];
    const birthday: string = form.value["birthday"];
    const location: string = form.value["location"];

    const ePassword: string = Md5.hashStr(password).toString();

    if(password == passwordConfirm) {
      this.userService.registerUser(username, ePassword, email, firstname, lastname, birthday, location).subscribe(
        data => this.redirect(data),
        error => console.log(error)
      );
    } else {
      console.log("Password doesnt match");
      //TODO show error
    }
  }

  redirect(data: any) {
    if(data["httpCode"] == 200) {
      //this.router.navigate(['/login']);
      this.printActivationKey(data["activation_key"]);
    }
  }

  printActivationKey(key: string) {
    this.activationKey = key;
    this.showActivationButton = true;
  }

  onClickActivate() {
    this.userService.activateUser(this.activationKey).subscribe(
      data => this.router.navigate(['/login']),
      error => console.log(error)
    );
  }

}
