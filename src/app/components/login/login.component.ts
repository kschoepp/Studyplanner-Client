import {Component} from "@angular/core";
import {UserService} from "../../services/rest/user.service";
import {NgForm} from "@angular/forms";
import {Router} from "@angular/router";
import {SessionService} from "../../services/storage/session.service";
import {Md5} from 'ts-md5/dist/md5';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [UserService]
})

export class LoginComponent {

  constructor(private sessionService: SessionService, private userService: UserService, private router: Router) {}

  onSubmit(form: NgForm) {
    const username: string = form.value["username"];
    const password: string = form.value["password"];

    const ePassword: string = Md5.hashStr(password).toString();
    //console.log(ePassword);

    this.userService.validateLogin(username, ePassword).subscribe(
      data => this.getDetails(data, username, ePassword),
      error => console.log(error)
    );
  }

  getDetails(data: any, username: string, password: string) {
    if(data["httpCode"] == 200) {
      this.userService.getUserDetails(username, password).subscribe(
        (data: JSON) => this.login(username, password, data),
        error => console.log(error)
      );
    }
  }

  login(username: string, password: string, data: JSON) {
    this.sessionService.createSession(Number(data["id"]), username, password, data["firstname"], data["lastname"], data["email"]);
    this.router.navigate(['/']);
  }

  showError(error: any) {

  }

}
