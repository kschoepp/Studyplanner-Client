import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from "@angular/forms";

import { AppComponent } from './app.component';
import { LoginComponent } from "./components/login/login.component";
import { NavbarComponent } from "./components/navbar/navbar.component";
import { DashboardComponent } from "./components/dashboard/dashboard.component";
import { TodoListComponent } from "./components/todolist/todolist.component";
import {AppRouter} from "./app.routing";
import {HttpClientModule} from "@angular/common/http";
import {CalendarComponent} from "./components/calendar/calendar.component";
import {CoursesComponent} from "./components/courses/courses.component";
import {ExamsComponent} from "./components/exams/exams.component";
import {ContactsComponent} from "./components/contacts/contacts.component";
import {RegisterComponent} from "./components/register/register.component";
import {SessionService} from "./services/storage/session.service";
import {TodoListItemComponent} from "./components/todolist/item/todolist-item.component";
import {KeyService} from "./services/utils/key.service";
import {CalendarModule} from "angular-calendar";
import {ExamItemComponent} from "./components/exams/item/exam-item.component";
import {CalendarItemComponent} from "./components/calendar/item/calendar-item.component";
import {TodolistDetailComponent} from "./components/todolist/detail/todolist-detail.component";
import {ContactsItemComponent} from "./components/contacts/item/contacts-item.component";
import {ConfigService} from "./services/utils/config.service";
import {CourseItemComponent} from "./components/courses/item/course-item.component";

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    NavbarComponent,
    DashboardComponent,
    TodoListComponent,
    TodoListItemComponent,
    TodolistDetailComponent,
    CalendarComponent,
    CalendarItemComponent,
    CoursesComponent,
    CourseItemComponent,
    ExamsComponent,
    ExamItemComponent,
    ContactsComponent,
    ContactsItemComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRouter,
    CalendarModule.forRoot()
  ],
  providers: [
    SessionService,
    KeyService,
    ConfigService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
